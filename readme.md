<!-- Este es un proyecto de uso didactico el cual fue desarrollado 
como práctica del Curso de Desarrollo Backend con Node.js, Express y MongoDB 
de la Universidad Austral. -->

Instrucciones para correr el proyecto:

- En la terminal usa el comando *npm install* para que descargues las dependencias que usa el proyecto.

- Luego, *npm run devstart* para inicializar el proyecto

- Para visualizar el proyecto, entras en tu navegador e ingresas a: localhost:5000

- Para correr las pruebas unitarias usas la instruccion *npm test*